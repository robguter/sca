import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// Declaramos las variables para jQuery
import * as $ from 'jquery';

@Component({
  selector: 'app-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css'],
})
export class MainNavComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver) {}
  public ngOnInit() {
    $(document).ready(function() {
      $('.titMnu').each(function(i) {
        $(this).click(function() {
          $('.cont:nth(' + i + ')').slideToggle();
        });
      });
    });
  }
  /*
  public mostrarCapa(e) {
    console.log(e);
    $(".cont:nth("+e+")").slideToggle();
  }
  */
}
