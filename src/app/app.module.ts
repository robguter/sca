import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app-routing.module';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { InicioComponent } from './components/inventario/inicio/inicio.component';
import { AlmacensComponent } from './components/inventario/almacens/almacens.component';
import { CliesComponent } from './components/inventario/clie/clie.component';
import { CxcsComponent } from './components/inventario/cxc/cxc.component';
import { CxpsComponent } from './components/inventario/cxp/cxp.component';
import { CprasComponent } from './components/inventario/cpra/cpra.component';
import { EstantesComponent } from './components/inventario/estantes/estantes.component';
import { ProdsComponent } from './components/inventario/prods/prods.component';
import { ProvsComponent } from './components/inventario/provs/provs.component';
import { FactsComponent } from './components/inventario/fact/fact.component';

import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './main-nav/main-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    AlmacensComponent,
    CliesComponent,
    CxcsComponent,
    CxpsComponent,
    CprasComponent,
    EstantesComponent,
    ProdsComponent,
    ProvsComponent,
    FactsComponent,
    MainNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
