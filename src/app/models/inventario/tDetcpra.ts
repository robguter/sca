export class Detcpra {
    _id?: string;
    idDetcpra: number;
    idCpra: number;
    idProd: string;
    cantDetcpra: number;
    precDetcpra: number;
    subtDetcpra: number;
    n?: number;
}
