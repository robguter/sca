export class Fact {
    _id?: string;
    idFact: number;
    idClie: number;
    fecFact: Date;
    mtoFact: number;
    staFact: boolean;
    n?: number;
}
