export class Cxp {
    _id?: string;
    idCxp: number;
    idProv: number;
    fecCxp: Date;
    nfcCxp: number;
    mtoCxp: number;
    staCxp: boolean;
    n?: number;
}
