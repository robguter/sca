export class Prod {
    _id?: string;
    idProd: string;
    nomProd: string;
    dscProd: string;
    cntProd: number;
    minProd: number;
    maxProd: number;
    cstProd: number;
    prcProd: number;
    staProd: boolean;
    idEst: number;
    n?: number;
}
