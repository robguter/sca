export class Detfact {
    _id?: string;
    idDetfact: number;
    idFact: number;
    idProd: string;
    cantDetfact: number;
    precDetfact: number;
    subtDetfact: number;
    n?: number;
}
