export class Cpra {
    _id?: string;
    idCpra: number;
    idProv: number;
    fecCpra: Date;
    mtoCpra: number;
    staCpra: boolean;
    n?: number;
}
