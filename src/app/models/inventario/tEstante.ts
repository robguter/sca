export class Estante {
    _id?: string;
    idEst: number;
    idAlm: number;
    dscEst: string;
    staEst: boolean;
    n?: number;
}
