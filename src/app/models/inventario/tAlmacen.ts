export class Almacen {
    _id?: string;
    idAlm: number;
    dscAlm: string;
    staAlm: boolean;
    n?: number;
}
