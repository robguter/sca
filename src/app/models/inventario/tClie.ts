export class Clie {
    _id?: string;
    idClie: number;
    cdrClie: string;
    nomClie: string;
    cttClie: string;
    dirClie: string;
    tlfClie: string;
    emaClie: string;
    staClie: boolean;
    n?: number;
}
