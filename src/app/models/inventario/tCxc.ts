export class Cxc {
    _id?: string;
    idCxc: number;
    idClie: number;
    fecCxc: Date;
    nfcCxc: number;
    mtoCxc: number;
    staCxc: boolean;
    n?: number;
}
