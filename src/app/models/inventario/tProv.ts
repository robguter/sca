export class Prov {
    _id?: string;
    idProv: number;
    cdrProv: string;
    nomProv: string;
    cttProv: string;
    dirProv: string;
    tlfProv: string;
    emaProv: string;
    staProv: boolean;
    n?: number;
}
