import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './components/inventario/inicio/inicio.component';
import { AlmacensComponent } from './components/inventario/almacens/almacens.component';
import { EstantesComponent } from './components/inventario/estantes/estantes.component';
import { ProdsComponent } from './components/inventario/prods/prods.component';

import { ProvsComponent } from './components/inventario/provs/provs.component';
import { CliesComponent } from './components/inventario/clie/clie.component';
import { CxpsComponent } from './components/inventario/cxp/cxp.component';
import { CxcsComponent } from './components/inventario/cxc/cxc.component';
import { CprasComponent } from './components/inventario/cpra/cpra.component';
import { FactsComponent } from './components/inventario/fact/fact.component';

export const appRoutes: Routes = [
  { path: '*', component: InicioComponent },
  { path: '', component: InicioComponent },
  { path: 'inicio', component: InicioComponent },
  { path: 'almacens', component: AlmacensComponent },
  { path: 'estantes', component: EstantesComponent },
  { path: 'prods', component: ProdsComponent },
  { path: 'provs', component: ProvsComponent },
  { path: 'clies', component: CliesComponent },
  { path: 'cxps', component: CxpsComponent },
  { path: 'cxcs', component: CxcsComponent },
  { path: 'cpras', component: CprasComponent },
  { path: 'facts', component: FactsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
