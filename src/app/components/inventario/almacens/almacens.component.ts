import { Component, OnInit } from '@angular/core';
import { AlmacenService } from '../../../services/inventario/almacen.service';
import { NgForm } from '@angular/forms';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-almacens',
  templateUrl: './almacens.component.html',
  styleUrls: ['./almacens.component.css']
})
export class AlmacensComponent implements OnInit {

  constructor(public servicio: AlmacenService) {

    this.servicio.obtAlmacens();
    this.servicio.obtUltAlmacen();
  }
  public TextoBtn: String = 'Guardar';
  ngOnInit() {
    this.resetForm();
    this.getUltAlm();
  }
  getAlmacen(id) {
    this.servicio.obtAlmacen(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.servicio.almacU = this.servicio.formData.idAlm;
      this.TextoBtn = 'Actualizar';
    });
  }
  getUltAlm() {
    this.servicio.obtUltAlmacen()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.almacU = parseInt(res);
    });
  }
  refresca() {
    this.servicio.obtAlmacens();
    this.getUltAlm();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idAlm : null,
      dscAlm : null,
      staAlm : true
    };
    this.servicio.almacU = null;
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrAlmacen(form);
    } else {
      this.actAlmacen(form);
    }
  }
  agrAlmacen(form: NgForm) {
    this.servicio.agrAlmacen(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /* this.toastr.success('Insertado satisfactoriamente', 'Datos registrados');*/
    });
  }
  eliAlmacen(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliAlmacen(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Datos eliminados');*/
    });
  }
  actAlmacen(form: NgForm) {
    this.servicio.actAlmacen(form.value).subscribe(res => {
      this.resetForm(form);
      delay(1);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Datos modificados');*/
    });
  }
}
