import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlmacensComponent } from './almacens.component';

describe('AlmacensComponent', () => {
  let component: AlmacensComponent;
  let fixture: ComponentFixture<AlmacensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlmacensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlmacensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
