import { Component, OnInit } from '@angular/core';

import { CxpService } from '../../../services/inventario/cxp.service';
import { ProvService } from '../../../services/inventario/prov.service';

import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-cxps',
  templateUrl: './cxp.component.html',
  styleUrls: ['./cxp.component.css']
})
export class CxpsComponent implements OnInit {

  constructor(public servicio: CxpService,
    public provService: ProvService) {

    this.servicio.obtCxps();
    this.provService.obtProvs();
    this.servicio.obtUltCxp();
  }
  public TextoBtn: String = 'Guardar';
  ngOnInit() {
    this.resetForm();
    this.getUltCxp();
  }
  getCxp(id) {
    this.servicio.obtCxp(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.servicio.cxpU = res.idCxp;
      this.TextoBtn = 'Actualizar';
    });
  }
  getUltCxp() {
    this.servicio.obtUltCxp()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.cxpU = parseInt(res); // + 1;
    });
  }
  refresca() {
    this.servicio.obtCxps();
    this.provService.obtProvs();
    this.getUltCxp();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idCxp: null,
      idProv: null,
      fecCxp: null,
      nfcCxp: null,
      mtoCxp: null,
      staCxp : true
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrCxp(form);
    } else {
      this.actCxp(form);
    }
  }
  agrCxp(form: NgForm) {
    this.servicio.agrCxp(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Cxpucto registrado');*/
    });
  }
  eliCxp(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliCxp(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Cxpucto eliminado');*/
    });
  }
  actCxp(form: NgForm) {
    this.servicio.actCxp(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Cxpucto modificado');*/
    });
  }
}
