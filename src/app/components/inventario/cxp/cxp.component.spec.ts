import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxcsComponent } from './cxc.component';

describe('CxcsComponent', () => {
  let component: CxcsComponent;
  let fixture: ComponentFixture<CxcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
