import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CprasComponent } from './cpra.component';

describe('CprasComponent', () => {
  let component: CprasComponent;
  let fixture: ComponentFixture<CprasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CprasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CprasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
