import { Component, OnInit } from '@angular/core';

import { CpraService } from '../../../services/inventario/cpra.service';
import { ProvService } from '../../../services/inventario/prov.service';

import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-cpras',
  templateUrl: './cpra.component.html',
  styleUrls: ['./cpra.component.css']
})
export class CprasComponent implements OnInit {

  constructor(public servicio: CpraService,
    public provService: ProvService) {

    this.servicio.obtCpras();
    this.provService.obtProvs();
    this.servicio.obtUltCpra();
  }
  public TextoBtn: String = 'Guardar';
  ngOnInit() {
    this.resetForm();
    this.getUltCpra();
  }
  getCpra(id) {
    this.servicio.obtCpra(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.servicio.cpraU = res.idCpra;
      this.TextoBtn = 'Actualizar';
    });
  }
  getUltCpra() {
    this.servicio.obtUltCpra()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.cpraU = parseInt(res); // + 1;
    });
  }
  refresca() {
    this.servicio.obtCpras();
    this.getUltCpra();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idCpra: null,
      idProv: null,
      fecCpra: null,
      mtoCpra: null,
      staCpra: true
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrCpra(form);
    } else {
      this.actCpra(form);
    }
  }
  agrCpra(form: NgForm) {
    this.servicio.agrCpra(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Cpraucto registrado');*/
    });
  }
  eliCpra(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliCpra(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Cpraucto eliminado');*/
    });
  }
  actCpra(form: NgForm) {
    this.servicio.actCpra(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Cpraucto modificado');*/
    });
  }
}
