import { Component, OnInit } from '@angular/core';

import { EstanteService } from '../../../services/inventario/estante.service';
import { AlmacenService } from '../../../services/inventario/almacen.service';

import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-estantes',
  templateUrl: './estantes.component.html',
  styleUrls: ['./estantes.component.css']
})
export class EstantesComponent implements OnInit {

  constructor(public servicio: EstanteService,
    public almacenService: AlmacenService) {
      this.servicio.obtEstantes();
      this.almacenService.obtAlmacens();
      this.servicio.obtUltEstante();
  }
  public TextoBtn: String = 'Guardar';
  ngOnInit() {
    this.resetForm();
    this.getUltEst();
  }

  getEstante(id) {
    this.servicio.obtEstante(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.servicio.estU = res.idEst;
      this.TextoBtn = 'Actualizar';
    });
  }
  getUltEst() {
    this.servicio.obtUltEstante()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.estU = parseInt(res); // + 1;
    });
  }
  refresca() {
    this.servicio.obtEstantes();
    this.almacenService.obtAlmacens();
    this.getUltEst();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idEst : null,
      idAlm : null,
      dscEst : null,
      staEst : true
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrEstante(form);
    } else {
      this.actEstante(form);
    }
  }
  agrEstante(form: NgForm) {
    this.servicio.agrEstante(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Estanteucto registrado');*/
    });
  }
  eliEstante(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliEstante(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Estanteucto eliminado');*/
    });
  }
  actEstante(form: NgForm) {
    this.servicio.actEstante(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Estanteucto modificado');*/
    });
  }
}
