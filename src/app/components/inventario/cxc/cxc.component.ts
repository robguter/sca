import { Component, OnInit } from '@angular/core';

import { CxcService } from '../../../services/inventario/cxc.service';
import { ClieService } from '../../../services/inventario/clie.service';

import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-cxcs',
  templateUrl: './cxc.component.html',
  styleUrls: ['./cxc.component.css']
})
export class CxcsComponent implements OnInit {

  constructor(public servicio: CxcService,
    public clieService: ClieService) {

    this.servicio.obtCxcs();
    this.clieService.obtClies();
    this.servicio.obtUltCxc();
  }

  public TextoBtn: String = 'Guardar';
  ngOnInit() {
    this.resetForm();
    this.getUltCxc();
  }
  getCxc(id) {
    this.servicio.obtCxc(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.servicio.cxcU = res.idCxc;
      this.TextoBtn = 'Actualizar';
    });
  }
  getUltCxc() {
    this.servicio.obtUltCxc()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.cxcU = parseInt(res); // + 1;
    });
  }
  refresca() {
    this.servicio.obtCxcs();
    this.clieService.obtClies();
    this.getUltCxc();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idCxc: null,
      idClie: null,
      fecCxc: null,
      nfcCxc: null,
      mtoCxc: null,
      staCxc : true
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrCxc(form);
    } else {
      this.actCxc(form);
    }
  }
  agrCxc(form: NgForm) {
    this.servicio.agrCxc(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Cxcucto registrado');*/
    });
  }
  eliCxc(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliCxc(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Cxcucto eliminado');*/
    });
  }
  actCxc(form: NgForm) {
    this.servicio.actCxc(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Cxcucto modificado');*/
    });
  }
}
