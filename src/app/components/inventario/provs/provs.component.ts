import { Component, OnInit } from '@angular/core';
import { ProvService } from '../../../services/inventario/prov.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-provs',
  templateUrl: './provs.component.html',
  styleUrls: ['./provs.component.css']
})
export class ProvsComponent implements OnInit {

  constructor(public servicio: ProvService) {

    this.servicio.obtProvs();
    this.servicio.obtUltProv();
  }
  public TextoBtn: String = 'Guardar';
  ngOnInit() {
    this.resetForm();
    this.getUltProv();
  }
  getProv(id) {
    this.servicio.obtProv(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.servicio.provU = res.idProv;
      this.TextoBtn = 'Actualizar';
    });
  }
  getUltProv() {
    this.servicio.obtUltProv()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.provU = parseInt(res); // + 1;
    });
  }
  refresca() {
    this.servicio.obtProvs();
    this.getUltProv();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idProv : null,
      cdrProv : null,
      nomProv : null,
      cttProv : null,
      dirProv : null,
      tlfProv : null,
      emaProv : null,
      staProv : true
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrProv(form);
    } else {
      this.actProv(form);
    }
  }
  agrProv(form: NgForm) {
    this.servicio.agrProv(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Provucto registrado');*/
    });
  }
  eliProv(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliProv(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Provucto eliminado');*/
    });
  }
  actProv(form: NgForm) {
    this.servicio.actProv(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Provucto modificado');*/
    });
  }
}
