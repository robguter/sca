import { Component, OnInit } from '@angular/core';

import { FactService } from '../../../services/inventario/fact.service';
import { ClieService } from '../../../services/inventario/clie.service';
import { ProdService } from '../../../services/inventario/prod.service';
import { DetfactService } from '../../../services/inventario/detfact.service';

import { NgForm } from '@angular/forms';
import { SummaryResolver, AotSummaryResolver } from '@angular/compiler';

@Component({
  selector: 'app-facts',
  templateUrl: './fact.component.html',
  styleUrls: ['./fact.component.css']
})
export class FactsComponent implements OnInit {

  constructor(public servicio: FactService,
    public clieService: ClieService,
    public prodServicio: ProdService,
    public DFServicio: DetfactService) {

    this.servicio.obtFacts();
    this.clieService.obtClies();
    this.prodServicio.obtProds();
    this.DFServicio.obtDetfacts();

  }

  public DetfactMt: Array<any> = [];
  public newDF: any = {};
  public cDetfact = 0;
  public mTotFact = 0;
  ngOnInit() {
    this.resetForm();
    this.getUltFact();
    this.getUltDetfact();
  }
  getClie(id) {
    this.clieService.obtClie(id)
    .subscribe(res => {
      this.clieService.cliet = res;
    });
  }
  getProd(id) {
    this.prodServicio.obtUnProd(id)
    .subscribe(res => {
      this.prodServicio.prodt = res;
      this.newDF.idDetfact = this.DFServicio.detfactU;
      this.newDF.idFact = this.servicio.factU;
      this.newDF.idProd = res.idProd;
      this.newDF.cantDetfact = res.cntProd;
      this.newDF.precDetfact = res.prcProd;
      this.newDF.mntoDetfact = res.prcProd * res.cntProd;
    });
  }
  addFieldValue() {
    this.DFServicio.detfactU += this.cDetfact;
    this.newDF.idDetfact = this.DFServicio.detfactU;
    this.mTotFact += this.newDF.mntoDetfact;
    this.DetfactMt.push(this.newDF);
    this.newDF = {};
    this.cDetfact += 1;
    this.resetFormD();
   }

   deleteFieldValue(index) {
    this.DetfactMt.splice(index, 1);
   }

  resetFormD(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.prodServicio.prodt = {
      _id: null,
      idProd : null,
      nomProd : null,
      dscProd : null,
      cntProd : null,
      minProd : null,
      maxProd : null,
      cstProd : null,
      prcProd : null,
      idEst : null,
      staProd : true
    };
  }
  getFact(id) {
    this.servicio.obtFact(id)
    .subscribe(res => {
      this.servicio.formData = res;
      // this.servicio.factU = this.servicio.formData.idFact;
    });
  }
  getUltFact() {
    this.servicio.obtUltFact()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.factU = parseInt(res);
    });
  }
  getUltDetfact() {
    this.DFServicio.obtUltDetfact()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.DFServicio.detfactU = parseInt(res);
    });
  }
  refresca() {
    this.servicio.obtFacts();
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idFact : null,
      idClie : null,
      fecFact : null,
      mtoFact : null,
      staFact : true
    };
    this.clieService.cliet = {
      _id: null,
      idClie: null,
      cdrClie: null,
      nomClie: null,
      cttClie: null,
      dirClie: null,
      tlfClie: null,
      emaClie: null,
      staClie: true
    };
    this.prodServicio.prodt = {
      _id: null,
      idProd : null,
      nomProd : null,
      dscProd : null,
      cntProd : null,
      minProd : null,
      maxProd : null,
      cstProd : null,
      prcProd : null,
      idEst : null,
      staProd : true
    };
    this.DFServicio.detfactt = {
      _id: null,
      idDetfact : null,
      idFact : null,
      idProd : null,
      cantDetfact : null,
      precDetfact : null,
      subtDetfact : null
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrFact(form);
    } else {
      this.actFact(form);
    }
  }
  agrFact(form: NgForm) {
    this.servicio.agrFact(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Factucto registrado');*/
    });
  }
  eliFact(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliFact(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Factucto eliminado');*/
    });
  }
  actFact(form: NgForm) {
    this.servicio.actFact(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Factucto modificado');*/
    });
  }
}
