import { Component, OnInit } from '@angular/core';

import { ClieService } from '../../../services/inventario/clie.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-clie',
  templateUrl: './clie.component.html',
  styleUrls: ['./clie.component.css']
})
export class CliesComponent implements OnInit {

  constructor(public servicio: ClieService) {
    this.servicio.obtClies();
  }
  public TextoBtn: String = 'Guardar';

  ngOnInit() {
    this.resetForm();
    this.getUltClie();
  }
  getClie(id) {
    this.servicio.obtClie(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.servicio.clieU = res.idClie;
      this.TextoBtn = 'Actualizar';
    });
  }
  getUltClie() {
    this.servicio.obtUltClie()
    .subscribe(res => {
      // tslint:disable-next-line:radix
      this.servicio.clieU = parseInt(res); // + 1;
    });
  }
  refresca() {
    this.servicio.obtClies();
    this.getUltClie();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idClie : null,
      cdrClie: null,
      nomClie: null,
      cttClie: null,
      dirClie: null,
      tlfClie: null,
      emaClie: null,
      staClie: true
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrClie(form);
    } else {
      this.actClie(form);
    }
  }
  agrClie(form: NgForm) {
    this.servicio.agrClie(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Cliente registrado');*/
    });
  }
  eliClie(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliClie(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Cliente eliminado');*/
    });
  }
  actClie(form: NgForm) {
    this.servicio.actClie(form.value).subscribe(res => {
      /*this.toastr.success('Actualizado satisfactoriamente', 'Cliente modificado');*/
      this.resetForm(form);
      this.refresca();
    });
  }
}
