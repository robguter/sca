import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CliesComponent } from './clie.component';

describe('CliesComponent', () => {
  let component: CliesComponent;
  let fixture: ComponentFixture<CliesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CliesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
