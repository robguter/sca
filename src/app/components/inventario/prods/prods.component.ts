import { Component, OnInit } from '@angular/core';

import { ProdService } from '../../../services/inventario/prod.service';
import { EstanteService } from '../../../services/inventario/estante.service';

import { NgForm } from '@angular/forms';
/*import { ToastrService } from 'ngx-toastr';*/

@Component({
  selector: 'app-prods',
  templateUrl: './prods.component.html',
  styleUrls: ['./prods.component.css']
})
export class ProdsComponent implements OnInit {

  constructor(public servicio: ProdService,
    public eservicio: EstanteService/*,
    private toastr: ToastrService*/) {

    this.servicio.obtProds();
    this.eservicio.obtEstantes();
  }
  public TextoBtn: String = 'Guardar';
  ngOnInit() {
    this.resetForm();
  }
  getProd(id) {
    this.servicio.obtProd(id)
    .subscribe(res => {
      this.servicio.formData = res;
      this.TextoBtn = 'Actualizar';
    });
  }
  refresca() {
    this.servicio.obtProds();
    this.TextoBtn = 'Guardar';
  }
  resetForm(form?: NgForm) {
    if (form != null) { form.resetForm(); }
    this.servicio.formData = {
      idProd : null,
      nomProd : null,
      dscProd : null,
      cntProd : null,
      minProd : null,
      maxProd : null,
      cstProd : null,
      prcProd : null,
      idEst : null,
      staProd : true
    };
  }
  onSubmit(form: NgForm) {
    event.preventDefault();
    if (form.value._id == null) {
      this.agrProd(form);
    } else {
      this.actProd(form);
    }
  }
  agrProd(form: NgForm) {
    this.servicio.agrProd(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Insertado satisfactoriamente', 'Producto registrado');*/
    });
  }
  eliProd(id) {
    const response = confirm('Está seguro de querer eliminar este registro?');
    if (!response) { return; }
    this.servicio.eliProd(id).subscribe(res => {
      this.resetForm();
      this.refresca();
      /*this.toastr.success('Eliminado satisfactoriamente', 'Producto eliminado');*/
    });
  }
  actProd(form: NgForm) {
    this.servicio.actProd(form.value).subscribe(res => {
      this.resetForm(form);
      this.refresca();
      /*this.toastr.success('Actualizado satisfactoriamente', 'Producto modificado');*/
    });
  }
}
