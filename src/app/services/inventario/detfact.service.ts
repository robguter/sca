import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { Detfact } from '../../models/inventario/tDetfact';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DetfactService {
  formData: Detfact;
  detfactM: Detfact[];
  DetfactMt: Detfact;
  detfactt: Detfact;
  detfactU: Detfact['idDetfact'];

  private domain = 'http://localhost:8080/api/detfact';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtDetfacts() {
    return this.http.get<Detfact[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.detfactM = res as Detfact[]);
  }
  /*************** Ultimo */
  obtUltDetfact() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno por id*/
  obtDetfact(id) {
    return this.http.get<Detfact>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnDetfact(id) {
    return this.http.get<Detfact>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrDetfact(formData: Detfact) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliDetfact(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actDetfact(formData: Detfact) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
