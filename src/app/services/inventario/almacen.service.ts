import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Almacen } from '../../models/inventario/tAlmacen';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AlmacenService {
  formData: Almacen;
  almacenM: Almacen[];
  almacU: Almacen['idAlm'];

  private domain = 'http://localhost:8080/api/almacen';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtAlmacens() {
    return this.http.get<Almacen[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.almacenM = res as Almacen[]);
  }
  /*************** Ultimo */
  obtUltAlmacen() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno por id*/
  obtAlmacen(id) {
    return this.http.get<Almacen>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnAlmacen(id) {
    return this.http.get<Almacen>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrAlmacen(formData: Almacen) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliAlmacen(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actAlmacen(formData: Almacen) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
