import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Prov } from '../../models/inventario/tProv';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class ProvService {
  formData: Prov;
  provM: Prov[];
  provU: Prov['idProv'];

  domain: String = 'http://localhost:8080/api/prov';

  constructor(private http: HttpClient) {}

  /*************** Obtiene */
  obtProvs() {
    return this.http.get<Prov[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.provM = res as Prov[]);
  }
  /*************** Ultimo */
  obtUltProv() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  obtProv(id) {
    return this.http.get<Prov>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnProv(id) {
    return this.http.get<Prov>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrProv(formData: Prov) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliProv(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actProv(formData: Prov) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
