import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Clie } from '../../models/inventario/tClie';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ClieService {
  formData: Clie;
  clieM: Clie[];
  cliet: Clie;
  clieU: Clie['idClie'];

  private domain = 'http://localhost:8080/api/clie';  // URL to web api

  constructor( private http: HttpClient ) { }
  /*************** Obtiene */
  obtClies() {
    return this.http.get<Clie[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.clieM = res as Clie[]);
  }
  /*************** Ultimo */
  obtUltClie() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno por id*/
  obtClie(id) {
    return this.http.get<Clie>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnClie(id) {
    return this.http.get<Clie>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrClie(formData: Clie) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliClie(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actClie(formData: Clie) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
