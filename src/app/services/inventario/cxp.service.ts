import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Cxp } from '../../models/inventario/tCxp';
import { Prov } from '../../models/inventario/tProv';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CxpService {
  formData: Cxp;
  cxpM: Cxp[];
  provM: Prov[];
  eformData: Prov;
  cxpU: Cxp['idCxp'];

  private domain = 'http://localhost:8080/api/cxp';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtCxps() {
    return this.http.get<Cxp[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.cxpM = res as Cxp[]);
  }
  /*************** Ultimo */
  obtUltCxp() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  obtCxp(id) {
    return this.http.get<Cxp>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnCxp(id) {
    return this.http.get<Cxp>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrCxp(formData: Cxp) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliCxp(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actCxp(formData: Cxp) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
