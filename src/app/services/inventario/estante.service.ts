import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Estante } from '../../models/inventario/tEstante';
import { Almacen } from '../../models/inventario/tAlmacen';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EstanteService {
  formData: Estante;
  estanteM: Estante[];
  estU: Estante['idEst'];
  almacenM: Almacen[];
  eformData: Almacen;

  private domain = 'http://localhost:8080/api/estante';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtEstantes() {
    return this.http.get<Estante[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.estanteM = res as Estante[]);
  }
  obtUltEstante() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  obtUnEstante(id) {
    return this.http.get<Estante>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  obtEstante(id) {
    return this.http.get<Estante>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrEstante(formData: Estante) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliEstante(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actEstante(formData: Estante) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
