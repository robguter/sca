import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Prod } from '../../models/inventario/tProd';
// import { Estante } from '../../models/inventario/tEstante';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProdService {
  formData: Prod;
  prodM: Prod[];
  prodt: Prod;
  // estanteM: Estante[];
  // eformData: Estante;

  private domain = 'http://localhost:8080/api/prod';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtProds() {
    return this.http.get<Prod[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.prodM = res as Prod[]);
  }
  obtProd(id) {
    return this.http.get<Prod>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  obtUnProd(id) {
    return this.http.get<Prod>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrProd(formData: Prod) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliProd(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actProd(formData: Prod) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
