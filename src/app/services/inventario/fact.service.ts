import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Fact } from '../../models/inventario/tFact';
// import { Clie } from '../../models/inventario/tClie';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class FactService {
  formData: Fact;
  factM: Fact[];
  factU: Fact['idFact'];

  private domain = 'http://localhost:8080/api/fact';  // URL to web api
  // private edomai = 'http://localhost:8080/api/clies';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtFacts() {
    return this.http.get<Fact[]>(`${this.domain}`)
    .toPromise().
    then(res => this.factM = res as Fact[]);
  }
  /*************** Ultimo */
  obtUltFact() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  obtFact(id) {
    return this.http.get<Fact>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnFact(id) {
    return this.http.get<Fact>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrFact(formData: Fact) {
    return this.http.post(`${this.domain}`, formData);
  }
  /************** Elimina */
  eliFact(id) {
    return this.http.delete(`${this.domain}/${id}`);
  }
  /************** Actualiza */
  actFact(formData: Fact) {
    return this.http.put(`${this.domain}`, formData);
  }
}
