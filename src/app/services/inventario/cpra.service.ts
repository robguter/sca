import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Cpra } from '../../models/inventario/tCpra';
// import { Prov } from '../../models/inventario/tProv';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CpraService {
  formData: Cpra;
  cpraM: Cpra[];
  cpraU: Cpra['idCpra'];
  // provM: Prov[];
  // eformData: Prov;

  private domain = 'http://localhost:8080/api/cpra';  // URL to web api
  // private edomai = 'http://localhost:8080/api/provs';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtCpras() {
    return this.http.get<Cpra[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.cpraM = res as Cpra[]);
  }
  /*************** Ultimo */
  obtUltCpra() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  obtCpra(id) {
    return this.http.get<Cpra>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnCpra(id) {
    return this.http.get<Cpra>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrCpra(formData: Cpra) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliCpra(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actCpra(formData: Cpra) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
