import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Cxc } from '../../models/inventario/tCxc';
import { Clie } from '../../models/inventario/tClie';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CxcService {
  formData: Cxc;
  cxcM: Cxc[];
  eformData: Clie;
  cxcU: Cxc['idCxc'];

  private domain = 'http://localhost:8080/api/cxc';  // URL to web api

  constructor( private http: HttpClient ) { }

  /*************** Obtiene */
  obtCxcs() {
    return this.http.get<Cxc[]>(`${this.domain}s`)
    .toPromise().
    then(res => this.cxcM = res as Cxc[]);
  }
  /*************** Ultimo */
  obtUltCxc() {
    return this.http.get<any>(`${this.domain}`)
    .pipe(map(res => res));
  }
  obtCxc(id) {
    return this.http.get<Cxc>(`${this.domain}s/${id}`)
    .pipe(map(res => res));
  }
  /*************** Obtiene uno */
  obtUnCxc(id) {
    return this.http.get<Cxc>(`${this.domain}/${id}`)
    .pipe(map(res => res));
  }
  /************** Agrega */
  agrCxc(formData: Cxc) {
    return this.http.post(`${this.domain}s`, formData);
  }
  /************** Elimina */
  eliCxc(id) {
    return this.http.delete(`${this.domain}s/${id}`);
  }
  /************** Actualiza */
  actCxc(formData: Cxc) {
    return this.http.put(`${this.domain}s`, formData);
  }
}
